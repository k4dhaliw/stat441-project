# Presentation for stat441

1. Kagglers use XGBOOST

    * Boosting is extremely successful
    * Extreme Boosting is Extremely Extremely Successful
    * History on Extreme Gradient Boosted Trees
    * Handles Collinearity 
    * Hand Wavey Certainty on Significants of new features

2. Data Leakage Concerns

3. Meta Features

    * Price residuals
    * Kmeans Clustering
    * Walkscore [todo]
    * Random Effect Model for Priors (better than an Average)

