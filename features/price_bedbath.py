from kaggle import *
from scipy.stats import boxcox

def price(train_test):

    train_test["price_log"] = np.log(train_test.price)

    for cat in ["manager_id", "building_id", "display_address"]:
        x = train_test.groupby([cat]).agg(
            {"price_log":["mean", "std"],
             "price":["mean", "std"]})

        cols = [cat +"_" + "_".join(c) for c in list(x.columns)]
        x.columns = [cat +"_" + "_".join(c) for c in list(x.columns)]

        x = train_test.join(x, on=cat, lsuffix="price")
        x = x[ cols + ["price_log", "price"]].fillna(1)

        x[cat + "_price_log_diff"] = x[cat + "_price_log_mean"] - x["price_log"]
        x[cat + "_price_log_diff_scaled"] = (x[cat + "_price_log_diff"]) / x[cat + "_price_log_std"]

        del x["price_log"]
        del x["price"]

        x.to_csv("f_price/price_{}_diff.csv".format(cat))

    x = train_test[["price"]].copy()

    x["price_perbed"] = x.price / train_test["bedrooms"]
    x.loc[x["price_perbed"] == max(x["price_perbed"])] = -1

    x["price_perbath"] = x.price / train_test["bathrooms"]
    x.loc[x["price_perbath"] == max(x["price_perbath"])] = -1

    x["price_perbedbath"] = x.price / (train_test["bathrooms"] + train_test["bedrooms"])
    x.loc[x["price_perbedbath"] == max(x["price_perbedbath"])] = -1

    x["rooms_nrooms"] = (train_test["bathrooms"] + train_test["bedrooms"])
    x["rooms_bath"] = train_test["bathrooms"]
    x["rooms_bed"] = train_test["bedrooms"]
    x["rooms_diff"] = train_test["bedrooms"] - train_test["bathrooms"]

    x["rooms_ratio"] = train_test["bedrooms"] / train_test["bathrooms"]
    x.loc[x["rooms_ratio"] == max(x["rooms_ratio"])] = -1
    del x["price"]

    return x

price(train_test).to_csv("f_price/price.csv")
