from tqdm import tqdm

n_data = 10
categorys = ["building_id", "manager_id"]

for category in categorys:
    print(category)

    from kaggle import *

    df = train_test[[category, "created", "interest_level"]].copy()
    df["low"] = (df["interest_level"] == "low").astype(int)
    df["high"] = (df["interest_level"] != "low").astype(int)

    df["created"] = pd.to_datetime(df["created"]).dt.dayofyear
    train_test["created"] = df["created"]

    del df["interest_level"]

    interests = ["low", "high"]
    priors = df[interests].sum() / df[interests].sum().sum()

    df = (df.
            sort_values("created").
            groupby([category, "created"]).
            agg(sum)[interests].
            reset_index("created"))

    nd1 = 1 + n_data
    npriors = n_data * priors

    idxs = set(df.index)

    for idx in tqdm(idxs):
        temp = df.loc[[idx]].copy()

        if len(temp) == 1:
            temp.loc[:,interests] = temp.loc[:,interests].fillna(0) + npriors
            temp[interests] /= nd1
        else:
            temp.loc[:,interests] = temp.loc[:,interests].cumsum(0).shift().fillna(0) + npriors
            n = temp[interests].sum(1)
            temp[interests] = temp.loc[:,interests].apply(lambda _: _/n)
        df.loc[[idx]] = temp.copy()

    df.reset_index(category, inplace=1)

    features = train_test[[category, "created"]].copy()
    features["listing_id"] = train_test.index
    features = pd.merge(df, features, left_on=[category, "created"], right_on=[category, "created"])
    features = features.set_index("listing_id")[interests]
    features.columns = [category + "_" + c for c in features.columns]

    features.to_csv("f_priors/mcode_bin_{}.csv".format(category))
