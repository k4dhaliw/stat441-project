from kaggle import *

def ind(df):
    df["missing_building_id"] = df.building_id == 0
    df["missing_latlong"] = df.latitude == 0
    return df[["missing_building_id", "missing_latlong"]]

ind(train_test).to_csv("f_ind/misc_indicators.csv")
