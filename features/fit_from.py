import pandas as pd
import xgboost as xgb
import numpy as np
import pickle
from kaggle import Xy


title = "march9_code"

SEED = 777

params = {
    'eta':.02,
    'gamma':1,
    'colsample_bytree':.7,
    'subsample':.7,
    'seed':0,
    'nthread':16,
    'objective':'multi:softprob',
    'eval_metric':'mlogloss',
    'num_class':3,
    'silent':1,
    'max_depth':8,
}

print("log - loading features")
train_X, test_X, y_train, listing_id, fnames = Xy()

print(train_X.shape)
print(test_X.shape)
print("n_features: ", len(fnames))

dtrain = xgb.DMatrix(data=train_X, label=y_train, feature_names=fnames)
dtest = xgb.DMatrix(data=test_X, feature_names=fnames)

print("log - saving model")
bst = xgb.Booster(params)
bst.load_model("../models/{}.model".format(title))

print("log - dumping model")
print(pd.Series(bst.get_score()).to_csv("../models/feat_{}.csv".format(title)))

print("log - predict phase")
preds = bst.predict(dtest)
preds = pd.DataFrame(preds)
cols = ['high', 'medium', 'low']
preds.columns = cols
preds['listing_id'] = listing_id
preds.to_csv('../models/predict_{}.csv'.format(title), index=None)
