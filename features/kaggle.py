import pandas as pd
import numpy as np
import os

from sklearn.preprocessing import OneHotEncoder
from scipy.sparse import hstack

def init():
    data_path = "~/data/"
    train_file = data_path + "train.json"
    test_file = data_path + "test.json"
    train_df = pd.read_json(train_file).set_index("listing_id")
    test_df = pd.read_json(test_file).set_index("listing_id")
    y_map = {'low': 0, 'medium': 1, 'high': 2}
    train_df['interest_level2'] = train_df['interest_level'].apply(lambda x: y_map[x])
    y_train = train_df.interest_level2
    train_test = pd.concat((train_df, test_df), axis=0)
    return test_df, train_df, y_train, train_test

test_df, train_df, y_train, train_test = init()

def Xy():
    test_df, train_df, y_train, train_test = init()
    train_test = train_test[[]].copy()

    fpath = "../features/"
    feature_subdirs = [fpath + p for p in os.listdir(fpath) if "f_" in p]
    features = [[p + "/" + f for f in os.listdir(p)] for p in feature_subdirs if len(os.listdir(p)) > 0 ]

    for f_class in features:
        for feature in f_class:
            df = pd.read_csv(feature).set_index("listing_id")
            if "onehot" in feature:
                df.columns = ["onehot_" + c for c in df.columns]
            train_test = train_test.join(df)

    for f in train_test.columns:
        print("feature: ", f)

    train_test = train_test.astype(np.float32)

    train_X = train_test.loc[train_df.index,:]
    test_X = train_test.loc[test_df.index,:]

    train_y = y_train.loc[train_df.index]
    listing_id = test_X.index.values

    return train_X, test_X, train_y, listing_id, train_test.columns
