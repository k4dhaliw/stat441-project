from kaggle import *
from sklearn.preprocessing import LabelEncoder

def tocats(train_test, cat):
    train_test[cat] = train_test[cat].str.lower()
    x = pd.DataFrame(train_test[cat].value_counts())
    x.index.name = cat
    x.columns = ["count"]
    new_cat = train_test[[cat]].replace({"0":"zero_id"})
    x = new_cat.join(x!=1, on=cat).fillna(True)
    x = x[cat] * x["count"]
    x = x.replace({"":"single_id"})
    print("fitting labels")
    labels = LabelEncoder().fit_transform(x)
    print("to dataframe")
    x = pd.DataFrame(labels, index=train_test.index, columns=[cat + "_labels"])
    return x

cats =  ["building_id",
         "manager_id",
         "street_address",
         "display_address"]

for c in cats:
    print(c)
    results = tocats(train_test, c)
    print("writing")
    results.to_csv("f_onehot/{}.csv".format(c))

train_test["listing_id"] = train_test.index
train_test[["listing_id"]].to_csv("f_onehot/listing_id.csv")

