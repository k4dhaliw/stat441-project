from kaggle import *

def add_times(train_test):
    train_test['Date'] = pd.to_datetime(train_test['created'])
    train_test['Year'] = train_test['Date'].dt.year
    train_test['Month'] = train_test['Date'].dt.month
    train_test['Day'] = train_test['Date'].dt.day
    train_test['Wday'] = train_test['Date'].dt.dayofweek
    train_test['Yday'] = train_test['Date'].dt.dayofyear
    train_test['hour'] = train_test['Date'].dt.hour
    return train_test[["Date", "Year", "Month", "Day", "Wday", "Yday", "hour"]]

df = pd.concat([add_times(df) for df in [train_df, test_df]], 0)
df.to_csv("f_created/times.py")
