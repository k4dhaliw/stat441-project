from kaggle import *
from sklearn.cluster import KMeans

def cluster(n):
    km = KMeans(n_clusters=n)
    labels = km.fit_transform(train_test[["latitude", "longitude"]])
    x =  pd.DataFrame(km.labels_,
                        index=train_test.index,
                        columns=["clusters_latlong_labels_n" + str(n)])
    return x

for n in [1000, 3000]:
    print("fitting cluster ", n)
    cluster(n).to_csv("f_onehot/cluster_n{}.csv".format(n))
