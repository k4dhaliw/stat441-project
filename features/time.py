from kaggle import *

train_test["created"] = pd.to_datetime(train_test["created"])
train_test["created_year"] = train_test["created"].dt.year
train_test["created_month"] = train_test["created"].dt.month
train_test["created_day"] = train_test["created"].dt.day
train_test["created_hour"] = train_test["created"].dt.hour
train_test["created_yday"] = train_test["created"].dt.dayofyear
train_test["created_yweek"] = train_test["created"].dt.weekofyear
train_test["created_time"] = train_test["created"].astype(int) // 1e8

df = train_test[["created_year",
                 "created_month",
                 "created_day",
                 "created_hour",
                 "created_yday",
                 "created_yweek",
                 "created_time"]]

df.to_csv("f_time/time.csv")
