from kaggle import *

def photos(df):
    df["n_photos"] = df["photos"].apply(len)
    return df[["n_photos"]]

photos(train_test).to_csv("f_photos/photos.csv")
