import pandas as pd
import xgboost as xgb
import numpy as np
import pickle

from kaggle import Xy

depth = 5
title = "mar15_d{depth}+sp(attr)+noquality".format(depth=depth)
best_rounds = None #2700 #None # IF NONE IT WILL CROSS VALIDATE

params = {
    'eta':.02,
    'gamma':1,
    'colsample_bytree':.5,
    'subsample':.7,
    'nthread':16,
    'objective':'multi:softprob',
    'eval_metric':'mlogloss',
    'num_class':3,
    'silent':1,
    'max_depth': depth,
    "min_child_weight":1,
    # 'booster': 'dart',
    # 'sample_type': 'uniform',
    # 'normalize_type': 'tree',
    # 'rate_drop': 0.1,
    # 'skip_drop': 0.5
}

print("log - loading features")
train_X, test_X, y_train, listing_id, fnames = Xy()

print("log - n_features: ", len(fnames))

dtrain = xgb.DMatrix(data=train_X.to_sparse(), label=y_train, feature_names=fnames)
dtest = xgb.DMatrix(data=test_X.to_sparse(), feature_names=fnames)

if not best_rounds:
    print("log - cv phase")
    bst = xgb.cv(params, dtrain, 10000, nfold=5, early_stopping_rounds=50, verbose_eval=50)
    best_rounds = np.argmin(bst['test-mlogloss-mean'])
    print("log - best rounds:", best_rounds)

print("log - fit phase")
bst = xgb.train(params, dtrain, best_rounds)

# print("log - saving model")
# bst.save_model("../models/{}.model".format(title))

print("log - dumping model")
print(pd.Series(bst.get_score()).to_csv("../models/feat_{}.csv".format(title)))

print("log - predict phase")
preds = bst.predict(dtest)
preds = pd.DataFrame(preds)
cols = ['low', 'medium', 'high']
preds.columns = cols
preds['listing_id'] = listing_id
preds[["listing_id", "low", "medium", "high"]].to_csv('../models/predict_{}.csv'.format(title), index=None)
