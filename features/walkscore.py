from kaggle import train_test
from sklearn.cluster import KMeans
import requests
import json
import percache
import time
import pandas as pd
import numpy as np
cache = percache.Cache("/home/ubuntu/data/walkscore_cache.dat")

@cache
def get_walkscore(lat, lon):
    url = 'http://api.walkscore.com/score?format=json&lat={lat}&lon={lon}&transit=1&bike=1&wsapikey=8728c8c7e1887a4f938e8e2ed7ff2a54'
    rspe = requests.get(url.format(lat=lat, lon=lon))
    time.sleep(1)
    return json.loads(rspe.text)

def get_kmeans_clusters(n_clusters):
    km = KMeans(n_clusters=n_clusters, random_state=1)
    labels = km.fit_transform(train_test[["latitude", "longitude"]])
    return km, km.labels_

def get_walkscore_map(clusters):
    km, labels = get_kmeans_clusters(clusters)
    ws_map = {}

    for i, c in enumerate(km.cluster_centers_):
        ws = get_walkscore(c[0], c[1])
        try:
            ws_map[i] = {}
            ws_map[i]['walk'] = ws.get('walkscore')
            ws_map[i]['bike'] = ws.get('bike').get('score') if ws.get('bike') is not None else None
            ws_map[i]['transit'] = ws.get('transit').get('score') if ws.get('transit') is not None else None
        except Exception as e:
            print(e, ws)
    return ws_map, labels

ws_map, labels = get_walkscore_map(1000)
# not keeping transit score since its always None
df = pd.DataFrame(index=train_test.index, columns=('cluster', 'walk', 'bike') )
df['cluster'] = labels
df['walk'] = df.cluster.apply(lambda x: ws_map[x]['walk'])
df['bike'] = df.cluster.apply(lambda x: ws_map[x]['bike'])

df.to_csv("f_latlong/walkscore.csv")
