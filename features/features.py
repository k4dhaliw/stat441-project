from kaggle import *
from sklearn.feature_extraction.text import CountVectorizer
from collections import Counter

train_test["n_features"] = train_test["features"].apply(len)
train_test["n_desc"] = train_test["description"].apply(lambda _: len(_.split(" ")))

df = train_test[["n_features", "n_desc"]]

features = train_test[["features"]].apply(lambda _: [list(map(str.strip, map(str.lower, x))) for x in _])
cc = Counter()
for x in features.features:
    cc.update(x)

xs = sorted([k for (k,v) in cc.items() if v > 5])

from collections import defaultdict

def clean(s):
    x = s.replace("-", "")
    x = x.replace(" ", "")
    x = x.replace("twenty four hour", "24")
    x = x.replace("24/7", "24")
    x = x.replace("24hr", "24")
    x = x.replace("24-hour", "24")
    x = x.replace("24hour", "24")
    x = x.replace("24 hour", "24")
    x = x.replace("common", "cm")
    x = x.replace("concierge", "doorman")
    x = x.replace("bicycle", "bike")
    x = x.replace("private", "pv")
    x = x.replace("deco", "dc")
    x = x.replace("decorative", "dc")
    x = x.replace("on-site", "os")
    return x

def feature_hash(x):
    cleaned = clean(x)
    k = cleaned[:4].strip()
    return k

cheap_hash = defaultdict(list)

for x in xs:
    cleaned = clean(x)
    k = cleaned[:4].strip()
    cheap_hash[k].append(x)

cheap_hash = {k:v[0] for (k,v) in cheap_hash.items()}
def dedup(features, hash2feat=cheap_hash):
    for feature in features:
        h = feature_hash(feature)
        if h in cheap_hash:
            yield cheap_hash[h]

cv = CountVectorizer(analyzer=lambda _: dedup(_, cheap_hash))
f = cv.fit_transform(features.features)
r = {v:k for (k,v) in cv.vocabulary_.items()}
features  = pd.DataFrame(f.todense(), index=train_test.index, columns=["feature_" + r[x] for x in range(len(r))])
df.join(features).to_csv("/home/ubuntu/features/f_features/features.csv")
