from kaggle import *

def percentiles(df, cat):
    cat_percentile = df[cat].value_counts().sort_values().cumsum()
    cat_percentile = cat_percentile/cat_percentile.max()
    cat_rank = pd.qcut(cat_percentile, 25).rank(method='dense').sort_values()
    df = df.join(cat_rank, on=cat, rsuffix="_rank")
    df = df.join(cat_percentile, on=cat, rsuffix="_percentile")
    return df[["{}_rank".format(cat), "{}_percentile".format(cat)]]

categorical_id = ["building_id", "manager_id", "street_address", "display_address"]
for cat in categorical_id:
    percentiles(train_test, cat).to_csv("f_percentile/{}.csv".format(cat))
