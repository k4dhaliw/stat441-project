import pandas as pd
import xgboost as xgb
import pickle

print("loading data")

x_train = pickle.load(open("/home/ubuntu/mat/x_train.m", "rb"))
y_train = pickle.load(open("/home/ubuntu/mat/y_train.m", "rb"))
x_test = pickle.load(open("/home/ubuntu/mat/x_test.m", "rb"))
listing_id = pickle.load(open("/home/ubuntu/mat/listing_id.m", "rb"))

SEED = 777
NFOLDS = 3

params = {
    'eta':.01,
    'colsample_bytree':.8,
    'subsample':.8,
    'seed':0,
    'nthread':16,
    'objective':'multi:softprob',
    'eval_metric':'mlogloss',
    'num_class':3,
    'silent':1,
    'max_depth':8,

}

dtrain = xgb.DMatrix(data=x_train, label=y_train)
dtest = xgb.DMatrix(data=x_test)


print("training")
bst = xgb.train(params, dtrain, 1100)
pickle.dump(bst, open("model_bst_mar8_1100trees.model", "wb"))

print("predicting")
preds = bst.predict(dtest)
preds = pd.DataFrame(preds)
cols = ['high', 'medium', 'low']
preds.columns = cols
preds['listing_id'] = listing_id
preds.to_csv('results_bst_mar8_1100trees.csv', index=None)
