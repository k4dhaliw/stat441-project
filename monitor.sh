#!/bin/bash
CPU=$(sar 1 5 | grep "Average" | sed 's/^.* //' | sed 's/\..*//g')
echo $CPU
if [ $CPU -gt 20 ]
then
  cat mail_content.html | /usr/sbin/sendmail -t
else
  echo "Normal"
fi
